## Введение
Сервис позволяет запускать тестовый стенд, состоящий из следующих контейнеров:

- bitrix (apache, php7, дистрибутив bitrix)
- mysql
- jmeter

## Зависимости
- Git

apt-get install -y git

- Docker

Используйте [инструкцию](https://docs.docker.com/install/linux/docker-ce/ubuntu)

- Docker-Compose

Используйте [инструкцию](https://docs.docker.com/compose/install)


### Начало работы
- Склонируйте репозиторий
```
git clone https://navinogradov@bitbucket.org/navinogradov/bitrix.git
```
- Запустите стенд
```
docker-compose up -d
```

Чтобы проверить, что все сервисы запустились посмотрите список запущенных контейнеров ```docker ps```.  

### Установка bitrix
Для установки bitrix откройте в браузере адрес http://127.0.0.1
Для настройки подключения к БД нужно использдовать следующие параметры:
```
Адрес сервера - mysql
Пользователь - root
Пароль - bitrix
База данных - bitrix
```

### Использование jmeter
Для использования jmeter нужно подключиться к контейнеру по ssh с включенным перенаправлением X11 ```ssh -X bitrix@127.0.0.1```

Для подключения используется следующий пользователь:
```
Пользователь - bitrix
Пароль - bitrix
```

